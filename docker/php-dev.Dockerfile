FROM debian:buster-slim

RUN apt update \
    && apt -y install ca-certificates \
                   apt-transport-https \
                   wget \
                   gnupg gnupg2 gnupg1 \
                   unzip \
                   git \
    && wget -q https://packages.sury.org/php/apt.gpg -O- | apt-key add - \
    && echo 'deb https://packages.sury.org/php buster main' > /etc/apt/sources.list.d/php.list\
    && apt update \
    && apt -y install php7.4-fpm \
                      php7.4-curl \
                      php7.4-xml \
                      php7.4-mbstring \
                      php7.4-gd \
                      php7.4-zip \
                      php7.4-mysql \
                      supervisor \
                      php-xdebug \
                      default-mysql-client \
                      iproute2\
                      curl
                      
RUN mkdir /run/php
RUN sed -i 's/error_log.*/error_log = \/proc\/self\/fd\/2/' /etc/php/7.4/fpm/php-fpm.conf
RUN sed -i 's/user =.*/user = app/' /etc/php/7.4/fpm/pool.d/www.conf
RUN sed -i 's/group =.*/group = app/' /etc/php/7.4/fpm/pool.d/www.conf
RUN sed -i 's/listen.*/listen = 9000/' /etc/php/7.4/fpm/pool.d/www.conf
RUN echo 'xdebug.remote_enable=1' >> /etc/php/7.4/fpm/php.ini \
 && echo 'xdebug.remote_port=9001' >> /etc/php/7.4/fpm/php.ini \
 && echo 'xdebug.remote_host=' >> /etc/php/7.4/fpm/php.ini
RUN echo 'xdebug.remote_enable=1' >> /etc/php/7.4/cli/php.ini \
 && echo 'xdebug.remote_port=9001' >> /etc/php/7.4/cli/php.ini \
 && echo 'xdebug.remote_host=' >> /etc/php/7.4/cli/php.ini

RUN wget https://getcomposer.org/download/1.9.0/composer.phar -O /usr/local/bin/composer \
    && chmod a+x /usr/local/bin/composer

RUN wget https://deployer.org/deployer.phar \
    && mv deployer.phar /usr/local/bin/dep \
    && chmod +x /usr/local/bin/dep

RUN useradd -m -s /bin/bash app \
    && usermod -a -G tty app
RUN mkdir /app && chown app:www-data /app

#USER app
WORKDIR /app

EXPOSE 9000

COPY ./docker/entrypoint.sh /entrypoint.sh
RUN chmod u+x /entrypoint.sh

ENTRYPOINT /entrypoint.sh
