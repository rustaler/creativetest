#!/bin/bash

# Добавляем адрес хоста в xdebug
sed -i "s/xdebug.remote_host=*.*.*.*/xdebug.remote_host=$(ip route show default | grep -oP '\d+.\d+.\d+.\d+')/" /etc/php/7.4/fpm/php.ini
sed -i "s/xdebug.remote_host=*.*.*.*/xdebug.remote_host=$(ip route show default | grep -oP '\d+.\d+.\d+.\d+')/" /etc/php/7.4/cli/php.ini

runuser -l app -c 'composer install -d /app'

host="mysql"
port="3306"

until curl http://"$host":"$port"; do
	sleep 1
done
php bin/console orm:schema-tool:drop --force
php bin/console orm:clear-cache:metadata
php bin/console orm:schema-tool:create
php bin/console fetch:trailers

#chown -R app:app /app/var/ 
chmod -R 777 /app/var/

# Далее запускаем supervisor
supervisord -n -c /etc/supervisor/supervisord.conf